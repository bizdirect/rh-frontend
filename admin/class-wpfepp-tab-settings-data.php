<?php

if(!class_exists('WPFEPP_Tab')){
    require_once 'class-wpfepp-tab.php';
}

class WPFEPP_Tab_Settings_Data extends WPFEPP_Tab
{
	/**
	 * Class constructor. Calls the parent constructor.
	 *
	 * @var string $version Plugin version.
	 * @var string $slug Tab slug.
	 * @var string $name Tab name.
	 **/
	function __construct($version, $slug, $name) {
		parent::__construct($version, $slug, $name);
	}

	/**
	 * Registers the actions of this class with WordPress. This function is called by add_actions of WPFEPP_Tab_Collection, which in turn is called by add_actions of WPFEPP_Plugin_Settings.
	 **/
	public function add_actions(){
		add_action( 'admin_init', array($this, 'create_options') );
		add_action( 'wp_ajax_wpfepp_export_forms', array( $this, 'export_section_callback' ) );
	}

	/**
	 * Creates the settings section, resgisters setting and adds settings fields. Uses WordPress' settings API.
	 *
	 * @see add_settings_section()
	 * @see register_setting()
	 * @see add_settings_field()
	 **/
	public function create_options(){
		$section 		= 'wpfepp_data_settings_section';
		$tab 			= 'wpfepp_data_settings_tab';
		$option_id 		= 'wpfepp_data_settings';
		$curr_settings 	= get_option($option_id);
		$callback 		= array( $this->renderer, 'render' );
		$args 			= array( 'group' => $option_id, 'curr' => $curr_settings);

		add_settings_section( $section, '', array( $this, 'section_callback' ), $tab );
		register_setting( $tab, $option_id, array( $this->validator, 'validate' ) );

	    add_settings_field(
	        'delete_on_uninstall', __('Delete All Data on Uninstallation', 'wpfepp-plugin'), $callback, $tab, $section,
	        array_merge(
				array(
					'desc' 	=> __('If you want to permanently remove this plugin then you might want to set this to true.', 'wpfepp-plugin'),
					'id' 	=> 'delete_on_uninstall',
					'type' 	=> 'checkbox'
				),
				$args
			)
	    );

		$export_section = 'wpfepp_export_forms_section';
		$export_tab = 'wpfepp_export_forms_tab';
		$export_option_id = 'wpfepp_export_forms';
		add_settings_section( $export_section, '', array( $this, 'export_section_callback' ), $export_tab );
	    add_settings_field(
	        'export_forms', __('Export forms', 'wpfepp-plugin'), $callback, $export_tab, $export_section,
	        array_merge(
				array(
					'desc' 	=> __( 'Copy the code from the field and save it as JSON file.', 'wpfepp-plugin'),
					'id' 	=> 'export_forms',
					'type' 	=> 'textarea',
					'rows'	=> 5,
					'cols'	=> 100
				),
				array( 'group' => $export_option_id )
			)
	    );
		
		$import_section = 'wpfepp_import_forms_section';
		$import_tab = 'wpfepp_import_forms_tab';
		$import_option_id = 'wpfepp_import_forms';
		add_settings_section( $import_section, '', array( $this, 'import_section_callback' ), $import_tab );
	    add_settings_field(
	        'import_forms', __('Import forms', 'wpfepp-plugin'), $callback, $import_tab, $import_section,
	        array_merge(
				array(
					'desc' 	=> __('Select local JSON file to import saved forms. IMPORTANT: All your current forms will be erased.', 'wpfepp-plugin'),
					'id' 	=> 'import_forms',
					'type' 	=> 'file',
					'accept' => '.json'
				),
				array( 'group' => $import_option_id )
			)
	    );
	}

	/**
	 * Outputs the contents of the tab with the help of WordPress' settings API.
	 **/
	public function display(){
	    ?>
	    <form method="post" action="options.php">
		    <?php
				settings_fields( 'wpfepp_data_settings_tab' );
				do_settings_sections( 'wpfepp_data_settings_tab' );
				submit_button( __('Save Settings', 'wpfepp-plugin'), 'primary', null, true );
			?>
		</form>
		
		
		<?php 
			//settings_fields( 'wpfepp_export_forms_tab' );
			do_settings_sections( 'wpfepp_export_forms_tab' );
			submit_button( __('Export', 'wpfepp-plugin'), 'primary', 'wpfepp_export_button', false ); 
		?>
		
		<form method="post" enctype="multipart/form-data">
		    <?php
				settings_fields( 'wpfepp_import_forms_tab' );
				do_settings_sections( 'wpfepp_import_forms_tab' );
				submit_button( __('Import', 'wpfepp-plugin'), 'primary', 'wpfepp_import_button', false );
				?>
		</form>
		<?php
			
	}

	public function section_callback($args){}
	
	/**
	 * Callbak export function
	 **/
	public function export_section_callback( $args ){
		if( isset( $_POST['action'] ) && $_POST['action'] == 'wpfepp_export_forms' ){
			$this->db->export_tables_to_json();
		}
	}
	
	/**
	 * Callbak import function
	 **/
	public function import_section_callback( $args ){
		if( isset( $_POST['wpfepp_import_button'] ) && isset( $_POST['_wpnonce'] ) && wp_verify_nonce($_POST['_wpnonce'], 'wpfepp_import_forms_tab-options' ) ){
			if( isset( $_FILES['wpfepp_import_forms'] ) && is_uploaded_file( $_FILES['wpfepp_import_forms']['tmp_name']['import_forms']['val'] ) ) {
				$path_to_file = $_FILES['wpfepp_import_forms']['tmp_name']['import_forms']['val'];
				$reponse = $this->db->import_tables_from_json( $path_to_file ); 
				if( $reponse ){
					?>
					<div class="notice notice-success is-dismissible"><p><?php _e( 'The forms were updated', 'wpfepp-plugin' ); ?></p></div>
					<?php
				}else{
					?>
					<div class="notice notice-error is-dismissible"><p><?php _e( 'Something went wrong', 'wpfepp-plugin' ); ?></p></div>
					<?php
				}
			}
		}
	}
	
}