<?php

if (class_exists('WC_Product')) {
	
	class WC_Product_Wpfepp_Package extends WC_Product {

		/**
		 * Initialize Package product.
		 */
		public function __construct( $product = 0 ) {
			$this->supports[] = 'ajax_add_to_cart';
			parent::__construct( $product );
		}
		
		/**
		 * Get internal type.
		 */
		public function get_type() {
			return 'wpfepp_package';
		}
		
		/**
		 * Get the add to url used mainly in loops.
		 */
		public function add_to_cart_url() {
			$url = $this->is_purchasable() && $this->is_in_stock() ? remove_query_arg( 'added-to-cart', add_query_arg( 'add-to-cart', $this->id ) ) : get_permalink( $this->id );

			return apply_filters( 'woocommerce_product_add_to_cart_url', $url, $this );
		}

		/**
		 * Get the add to cart button text.
		 */
		public function add_to_cart_text() {
			$text = $this->is_purchasable() && $this->is_in_stock() ? __( 'Add to cart', 'wpfepp-plugin' ) : __( 'Read more', 'wpfepp-plugin' );

			return apply_filters( 'woocommerce_product_add_to_cart_text', $text, $this );
		}

		/**
		 * Get the add to cart button text description - used in aria tags.
		 */
		public function add_to_cart_description() {
			/* translators: %s: Product title */
			$text = $this->is_purchasable() && $this->is_in_stock() ? __( 'Add &ldquo;%s&rdquo; to your cart', 'wpfepp-plugin' ) : __( 'Read more about &ldquo;%s&rdquo;', 'wpfepp-plugin' );

			return apply_filters( 'woocommerce_product_add_to_cart_description', sprintf( $text, $this->get_name() ), $this );
		}
	}
}