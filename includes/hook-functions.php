<?php
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Rehub customizations
 */
function rh_change_login_url_class( $class ){
	if( WPFEPP_REHUB && rehub_option( 'userlogin_enable' ) == '1' ){
		$class = 'act-rehub-login-popup';
	}
	return $class;
}
add_filter( 'wpfepp_login_url_class', 'rh_change_login_url_class' );

/**
 * Pass Map data to Geo My WP plugin
 */
function rh_gmw_pass_map_data( $post_id, $custom_fields ) {
	if(empty($post_id) || $post_id == '-1')
		return;
	$user_id = get_current_user_id();
	$address = !empty($custom_fields['_rh_gmw_map_hidden_adress']) ? $custom_fields['_rh_gmw_map_hidden_adress'] : '';
	if( !empty($address) && function_exists( 'gmw_update_location' ) ) {
		gmw_update_post_location( $post_id, stripslashes( $address ), $user_id, true );
	}
}
add_action( 'wpfepp_before_update_custom_field', 'rh_gmw_pass_map_data', 10, 2 );