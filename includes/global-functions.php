<?php
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**  
 * Run Paid form addon and WC Product type 'wpfepp_package'
 */
function wpfepp_wc_post_package(){
	$data_settings = get_option('wpfepp_payment_settings');
	if( $data_settings && $data_settings['turn_on_payment'] ){
		$current_ver = get_option('external_updates-rh-frontend');
		if( $current_ver && $current_ver->checkedVersion <= '3.7.2' ){
			global $wpdb;
			$terms_table = $wpdb->prefix ."terms";
			$wpfepp_terms = $wpdb->get_results("SELECT * FROM $terms_table WHERE name = 'rh-submit-package'", ARRAY_A);
			foreach( $wpfepp_terms as $wpfepp_term ){
				if( $wpfepp_term['name'] == 'rh-submit-package' || $wpfepp_term['name'] == 'rh-submit-package' ){
					$result = $wpdb->update( $terms_table, array( 'name' => 'wpfepp_package', 'slug' => 'wpfepp_package' ), array( 'term_id' => $wpfepp_term['term_id'] ) );
				}
			}
		}
		return true;
	}else{
		return false;
	}
}

/**
 * Creates an associative array of roles in which each element has role slug as the key and role name as value.
 */
function wpfepp_get_roles(){
	global $wp_roles;
	$roles = $wp_roles->roles;
	$rtn_arr 	= array();
	foreach ($roles as $key => $role) {
		$rtn_arr[$key] = $role['name'];
	}
	return $rtn_arr;
}

/**
 * Checks if the current user has a role for which the value of in the passed array is 1.
 */
function wpfepp_current_user_has( $roles ){
	$current_user = wp_get_current_user();
	foreach ( $current_user->roles as $role ) {
		if( isset($roles[$role]) && $roles[$role] )
			return true;
	}
	return false;
}

/**
 * Prints a list of roles in our special checkbox format.
 */
function wpfepp_print_roles_checkboxes($name, $checked){
	$roles = wpfepp_get_roles();
	?>
		<?php foreach ($roles as $key => $role): ?>
			<input type="hidden" name="<?php echo $name; ?>[<?php echo $key; ?>]" value="0" />
			<input type="checkbox" id="<?php echo $name; ?>[<?php echo $key; ?>]" name="<?php echo $name; ?>[<?php echo $key; ?>]" value="1" <?php if( isset($checked[$key]) ) checked( $checked[$key] ); ?> />
			<label for="<?php echo $name; ?>[<?php echo $key; ?>]"><?php echo $role; ?></label>
			<br/>
		<?php endforeach; ?>
	<?php
}

/**
 * Fetches an array of roles from wpfepp_get_roles() and convert it into a settings array.
 */
function wpfepp_prepare_default_role_settings(){
	$rtn_arr = array();
	global $wp_roles;
	$roles = $wp_roles->roles;
	foreach ($roles as $key => $role) {
		$rtn_arr[$key] = false;
	}
	return $rtn_arr;
}

/**
 * A recursive function that checks an array for missing keys. If any are found, inserts default values from the second array.
 */
function wpfepp_update_array($current, $default) {
	$current = ($current && is_array($current)) ? $current : array();
	foreach ($default as $key => $value) {
		if( !array_key_exists($key, $current) ){
			$current[$key] = $value;
		}
		elseif( is_array( $value ) ){
			$current[$key] = wpfepp_update_array( $current[$key], $value );
		}
	}
	return $current;
}

function wpfepp_update_form_fields( $current, $default, $default_custom ) {
	$current = wpfepp_update_array( $current, $default );
	foreach ($current as $key => $field) {
		if( $field['type'] == 'custom_field' )
			$current[$key] = wpfepp_update_array( $field, $default_custom );
	}
	return $current;
}

/**
 * Checks to see if a field is supported by the current post type and theme.
 */
function wpfepp_is_field_supported( $field_type, $post_type ) {
	
	if($field_type == 'thumbnail' ) {
		return ( post_type_supports($post_type, 'thumbnail') && get_theme_support('post-thumbnails') );
	}
	elseif($field_type == 'post_formats') {
		$formats = get_theme_support('post-formats');
		return ( post_type_supports($post_type, 'post-formats') && is_array($formats) && count($formats) && is_array($formats[0]) && count($formats[0]) );
	}
	elseif($field_type == 'content'){
		return post_type_supports($post_type, 'editor');
	}
	elseif($field_type == 'title' || $field_type == 'excerpt') {
		return post_type_supports($post_type, $field_type);
	}
	elseif(($field_type == 'sku' || $field_type == 'price' || $field_type == 'sale_price' || $field_type == 'product_options') && !class_exists('Woocommerce')) {
		return false;
	}
	return true;
}

/** 
 * Multiple choices
 */
function wpfepp_choices( $str ){
	$choices = array();

	if(empty($str))
		return $choices;
	$lines = explode("\n", $str);
	$count = 0;
	foreach ($lines as $line) {
		if(!empty($line)){
			$line_val = explode("|", $line);
			if(count($line_val) > 1){
				$choices[$count]['key'] = $line_val[0];
				$choices[$count]['val'] = $line_val[1];
			}
			else{
				$choices[$count]['key'] = $line_val[0];
				$choices[$count]['val'] = $line_val[0];
			}
			$count++;
		}
	}
	return $choices;
}

/**
 * Output the html of a form and includes the necessary scripts and stylesheets.
 */
function wpfepp_submission_form( $form_id ) {
	echo do_shortcode( sprintf('[wpfepp_submission_form form="%s"]', $form_id) );
}

/**
 * Output the html of a post table and includes the necessary scripts and stylesheets.
 */
function wpfepp_post_table( $form_id ) {
	echo do_shortcode( sprintf('[wpfepp_post_table form="%s"]', $form_id) );
}

/**
 * Gets post type names
 */
function wpfepp_get_post_types() {
	$types = get_post_types( array('show_ui'=>true), 'names', 'and' );
	unset( $types['attachment'] );
	return $types;
}

function wpfepp_get_post_type_settings() {
	$settings = array();
	$types = wpfepp_get_post_types();
	foreach ( $types as $key => $type ) {
		$settings[$type] = false;
	}
	return $settings;
}

if( !function_exists( 'ot_get_media_post_ID' ) ) {
	function ot_get_media_post_ID() {
		return -1;
	}
}

/**
 * Get WC attribute taxonomies.
 */
function wpfepp_get_attribute_taxonomies() {
	$arr_names = array();
	if ( false === ( $attribute_taxonomies = get_transient( 'wc_attribute_taxonomies' ) ) ) {
		global $wpdb;
		$attributes_table = $wpdb->prefix ."woocommerce_attribute_taxonomies";
		$attribute_taxonomies = $wpdb->get_results( "SELECT * FROM $attributes_table order by attribute_name ASC" );
		set_transient( 'wc_attribute_taxonomies', $attribute_taxonomies );
	}
	foreach( $attribute_taxonomies as $attribute_taxonomie ) {
		$arr_names[] = 'pa_' . $attribute_taxonomie->attribute_name;
	}
	return $arr_names;
}

/** 
 * Save Product attribut options
 */
function wpfepp_product_attributes_options( $post_id, $tax_attrs ) {
	if( empty( $post_id ) )
		return;
	
	$atts = array();
	if( !empty( $tax_attrs ) ) {
		for( $i = 0; $i < count( $tax_attrs ); ++$i ) {
			$tax_name = sanitize_title( $tax_attrs[$i] );
			$atts[ $tax_name ] = array(
				'name' => $tax_name,
				'value' => '',
				'position' => $i,
				'is_visible' => 1,
				'is_variation' => 1,
				'is_taxonomy' => 1
			);
		}
	} 
	/** 
	* Filter attribut options
	*/
	$attributes = apply_filters( 'wpfepp_product_attributes_options', $atts, $post_id, $tax_attrs );
	update_post_meta( $post_id, '_product_attributes', $attributes );
}

/* 
 * Takes multiple media IDs | ID | URL and returns preview HTML for them
 */
function wpfepp_media_preview_html( $media_data, $media_type ) {
	
	if ( ! $media_data )
		return;
	$html = '';
	if ( $media_type == 'attids' ) {
		foreach ( explode( ',', $media_data ) as $id ) {
			$html .= wp_get_attachment_image( trim( $id ), 'thumbnail');
		}
	} 
	if ( $media_type == 'attid' ) {
		$html = ( $media_data ) ? wp_get_attachment_image( $media_data, 'thumbnail', true ) : '';
	}
	if ( $media_type == 'atturl' ) {
		$id = wpfepp_get_attachment_id_by_url( $media_data );
		$html = ( $id ) ? wp_get_attachment_image( $id, 'thumbnail', true ) : '';
	}
	return $html;
}

/* 
 * Retrives Attachment ID by File URL
 */
function wpfepp_get_attachment_id_by_url( $url ) {

	$parsed_url  = explode( parse_url( WP_CONTENT_URL, PHP_URL_PATH ), $url );
	$this_host = str_ireplace( 'www.', '', parse_url( home_url(), PHP_URL_HOST ) );
	$file_host = str_ireplace( 'www.', '', parse_url( $url, PHP_URL_HOST ) );
	
	if ( ! isset( $parsed_url[1] ) || empty( $parsed_url[1] ) || ( $this_host != $file_host ) ) {
		return;
	}
	
	global $wpdb;
	$posts_table = $wpdb->prefix ."posts";
	$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $posts_table WHERE guid RLIKE %s;", $parsed_url[1] ) );
	return $attachment[0];
}

/**
 * Runs on update of the plugin
 */
function wpfepp_check_update() {
	require plugin_dir_path( __FILE__ ) . 'class-update-checker.php';
}
?>