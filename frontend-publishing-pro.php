<?php
/*
Plugin Name: RH Frontend Publishing Pro
Plugin URI: https://bitbucket.org/bizdirect/rehub-frontend-pro
Description: Allow your users to create, edit and delete posts directly from the WordPress frontend area.
Author: Wpsoul.com
Author URI: http://wpsoul.com
Version: 3.7.3
Requires at least: 4.4.0
Tested up to: 4.9.6
WC requires at least: 3.0.0
WC tested up to: 3.4.2
Text Domain: wpfepp-plugin
Domain Path: /languages/
*/

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'WPFEPP_VERSION', '3.7.3' );
define( 'WPFEPP_FILE', __FILE__ );
define( 'WPFEPP_DIR', dirname( WPFEPP_FILE ) . '/' );
define( 'WPFEPP_SLUG', 'rh-frontend' );
define( 'WPFEPP_REPO', 'http://wpsoul.net/plugins/' );

if( !defined( 'WPFEPP_REHUB' ) ){
	if( 'rehub' == get_option( 'template' ) ){
		define( 'WPFEPP_REHUB', true );
	} else {
		define( 'WPFEPP_REHUB', false );
	}
}

require WPFEPP_DIR . 'includes/global-functions.php';
require WPFEPP_DIR . 'includes/hook-functions.php';
if( wpfepp_wc_post_package() ){
	require WPFEPP_DIR . 'includes/paid-functions.php';
}

function wpfepp_run_plugin() {
	require_once WPFEPP_DIR . 'includes/class-frontend-publishing-pro.php';
	$wpfepp = new Frontend_Publishing_Pro( WPFEPP_VERSION );
	$wpfepp->run();
}

wpfepp_run_plugin();
wpfepp_check_update();

/**
 * Loads the plugin's text domain for localization.
 */
function wpfepp_load_plugin_textdomain() {
	load_plugin_textdomain( 'wpfepp-plugin', FALSE, basename( dirname( WPFEPP_FILE ) ) . '/languages/' );
}
add_action( 'plugins_loaded', 'wpfepp_load_plugin_textdomain' );

/**
 * Uses do_action to run plugin activation and initialization functions.
 */
function wpfepp_activation(){
	do_action( 'wpfepp_activation' );
}
register_activation_hook( WPFEPP_FILE, 'wpfepp_activation' );

/**
 * Uses do_action to run hooked functions when plugin is uninstalled.
 */
function wpfepp_uninstall(){
	do_action( 'wpfepp_uninstall' );
}
register_uninstall_hook( WPFEPP_FILE, 'wpfepp_uninstall' );

?>