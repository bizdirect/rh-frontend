jQuery(document).ready(function ($) {
	$('#wpfepp-dismiss-nag').click(function (e) {
		e.preventDefault();
		$(this).closest('.updated').hide();
		$.ajax({ type:'POST', url: ajaxurl, data: { action: 'wpfepp_dismiss_nag' } });
	});
	
	$(".wpfepp-op-sidebar .wpfepp-rehub-fields").on("click", "li", function(e){
		e.preventDefault();
		var clickLi = $(this).data("field");
		var targetContainer = $(this).parent();
		var targetParent = targetContainer.parent().prev().find("form input[name=meta_key]");
		targetParent.val(clickLi);
	});
	
	$("#wpfepp_export_button").click(function(e){
		e.preventDefault();
		var $this = $(this);
			$.post({
				type: "POST",
				url: ajaxurl,
				data: {
					action: 'wpfepp_export_forms',
				},
				success: function( result ){
					var ExportField = $(".wpfepp-op").find(".wpfepp_export_forms");
					
					ExportField.text(result);
					
					ExportField.click(function(){
						ExportField.select();
						document.execCommand('copy');
						ExportField.attr( 'title', 'Copied to clipboard' );
					});
				},
			});

		return false;
	});
});